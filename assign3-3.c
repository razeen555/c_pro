#include <stdio.h>
int main() {
    int i, reverce = 0, reminder;
    printf("Enter an integer: ");
    scanf("%d", &i);
    while (i!= 0) {
        reminder = i % 10;
        reverce = reverce * 10 + reminder;
        i /= 10;
    }
    printf("Reversed number = %d", reverce);
    return 0;
}
